#include "linked_list.h"

typedef struct list l_list;

l_list* iterate(int s, int n, int (*func) (int));
int foldl(int a, int (*func) (int, int), l_list* array);
l_list* map(int (*func) (int), l_list* array);
void foreach(l_list* array, void (*func) (int));
void map_mut(int (*func) (int), l_list* array);
