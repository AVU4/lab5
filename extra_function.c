#include "linked_list.h"
#include <stdio.h>
#include <stddef.h>
#include <stdbool.h>

typedef struct list {
	int value;
	l_list* next;
}l_list;


void foreach(l_list* array, void (*func) (int) ){
	size_t length = list_length(array);
	size_t i;
	for (i = 0; i < length; i ++)
		func(list_get(array, i));
}
	
l_list* map(int (*func) (int), l_list* array) {
	size_t length = list_length(array);
	l_list* head = NULL;
	size_t i;
	for (i = 0; i < length; i ++) {
		int new_value = func(list_get(array, i));
		list_add_back(&head, new_value);
	}
	return head;
}

int foldl(int a, int (*func) (int, int), l_list* array) {
	size_t length = list_length(array);
	size_t i;
	for (i = 0; i < length; i ++)
		a = func(list_get(array, i), a);
	return a;
}

void map_mut(int (*func) (int), l_list* array) {
	size_t length = list_length(array);
	size_t i;
	for (i = 0; i < length; i ++) {
		int new_value = func(list_get(array, i));
		list_set_value(array, i, new_value);
	}
}

l_list* iterate(int s, int n, int (*func) (int)){
	l_list* head = NULL;
	size_t i;
	for (i = 0; i < n; i ++){
		head = list_add_front(head, s);
		s = func(s);
	}
	return head;
}
