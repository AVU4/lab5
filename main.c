#include "linked_list.h"
#include "extra_function.h"
#include <stdio.h>
#include <stddef.h>
#include <limits.h>

static void print_row(int elem) {
	printf("%d ", elem);
}

static void print_column(int elem) {
	printf("%d\n", elem);
}

static int square(int elem) {
	return elem * elem > INT_MAX ? 0 : elem * elem;
}

static int cube(int elem) {
	return elem * elem * elem > INT_MAX ? 0 : elem * elem * elem;
}

static int sum(int elem, int a) {
	return elem + a > INT_MAX && elem + a < INT_MIN ? 0 : elem + a;
}

static int min(int elem, int a) {
	return elem > a ? a : elem;
}

static int max(int elem, int a) {
	return elem > a ? elem : a;
}

static int abs(int elem) {
	if (elem == INT_MIN) return 0;
	return elem > 0 ? elem : -1 * elem;
}

static int pow(int elem) {
	return elem * 2 > INT_MAX ? 0 : elem * 2;
}



int main( void ) {
	l_list* array;
	printf("Создаю массив\n");
	array = create_list();
	
	//foreach output
	void (*f_print_row) (int) = &print_row;
	void (*f_print_column) (int) = &print_column;
	printf("Вывод элементов строкой\n");
	foreach(array, print_row);
	printf("\n");
	printf("Вывод элементов столбцом\n");
	foreach(array, f_print_column);

	//map x^2 and x^3
	int (*f_square) (int) = &square;
	int (*f_cube) (int) = &cube;
	printf("Возведение в квадрат всех элементов массива\n");
	l_list* new_array = map(f_square, array);
	print_list(new_array);
	printf("\n");
	printf("Возведение в кубе всех элементов массива\n");
	new_array = map(f_cube, new_array);
	print_list(new_array);
	printf("\n");

	//foldl sum, max and min
	int (*f_sum) (int, int) = &sum;
	int (*f_max) (int, int) = &max;
	int (*f_min) (int, int) = &min;
	printf("The sum is %d\n", foldl(0, f_sum, array));
	printf("The min is %d\n", foldl(list_get(array, 0), f_min, array));
	printf("The max is %d\n", foldl(list_get(array, 0), f_max, array));

	//map_mut |x|
	printf("Создание нового массива для проверки функции модуля\n");
	new_array = create_list();
	int (*f_abs) (int) = &abs;
	map_mut(f_abs, new_array);
	print_list(new_array);
	printf("\n");

	//iterate 2*x
	int (*f_pow) (int) = &pow;
	new_array = iterate(1, 10, f_pow);
       	print_list(new_array);
	printf("\n");

	//save and load
	const char* filename = "output.txt";
	printf("Сохранение массива new_array в файл output.txt\n");
	save(new_array, filename);
	printf("Загрузка массива array из файла output.txt\n");
	load(&array, filename);
	print_list(array);
	printf("\n");

	//serialize and deserialize
	const char* filename_bin = "output.bin";
	printf("Сериализация массива array в файл output.bin\n");
	serialize(array, filename_bin);
	new_array = NULL;
	printf("Обратный процесс в массив new_array\n");
	deserialize(&new_array, filename_bin);
	print_list(new_array);
	printf("\n");

	list_free(array);
	list_free(new_array);
	return 0;
}

